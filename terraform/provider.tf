terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.2"
    }
  }
  required_version = ">= 0.13"
}

provider "helm" {
  kubernetes {
    config_paths = [
      var.KUBECONFIG
    ]
  }
}
