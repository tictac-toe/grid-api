resource "helm_release" "grid-api" {
  name      = "grid-api"
  chart     = "../helm-chart/grid-api"
  version   = "0.1.0"
  namespace = var.KUBE_NAMESPACE

  values = [
    "${file("conf/values.yaml")}"
  ]

  # Image parameters
  set {
    name  = "image.repository"
    value = var.GITLAB_REGISTRY_URL
  }
  set {
    name  = "image.tag"
    value = var.GITLAB_REGISTRY_IMAGE_TAG
  }

  # Postgres secrets
  set {
    name  = "secrets.DATABASE_PASSWORD"
    value = var.DATABASE_PASSWORD
  }
}
