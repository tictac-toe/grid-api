#!/usr/bin/env python

""" Manager """

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager  # class for handling a set of commands
from grid.grid import DB, app

MIGRATE = Migrate(app, DB)
MANAGER = Manager(app)

MANAGER.add_command('db', MigrateCommand)


if __name__ == '__main__':
    MANAGER.run()
