#!/usr/bin/env python

""" Grid
    Create app function which permit to create Flask app object
    and initialize database and routes
"""

from flask_api import FlaskAPI
from flask_cors import CORS
from grid.views import (delete_grid, get_all_grid,
                        get_grid, post_grid,
                        put_grid, api_root)

from . import DB

app = FlaskAPI(__name__)
CORS(app)
app.config.from_object('grid.settings')
DB.init_app(app)

# Routes
app.add_url_rule('/', view_func=api_root, methods=['GET'])
app.add_url_rule('/grids', view_func=get_all_grid, methods=['GET'])
app.add_url_rule('/grid/<int:grid_id>', view_func=get_grid, methods=['GET'])
app.add_url_rule('/grid', view_func=post_grid, methods=['POST'])
app.add_url_rule('/grid/<int:grid_id>', view_func=put_grid, methods=['PUT'])
app.add_url_rule('/grid/<int:grid_id>', view_func=delete_grid,
                 methods=['DELETE'])
